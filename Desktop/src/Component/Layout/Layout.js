import React from "react";
import { NavLink } from "react-router-dom";
import "./Layout.css";

const Layout = (props) => {
  return (
    <>
      <header className="header">
        <div className="container">
          <NavLink to="/">Dishes</NavLink>
          <NavLink to="/orders">Orders</NavLink>
        </div>
      </header>
      {props.children}
    </>
  );
};

export default Layout;
