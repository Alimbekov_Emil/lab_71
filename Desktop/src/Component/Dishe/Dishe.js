import React from "react";
import { NavLink } from "react-router-dom";
import "./Dishe.css";

const Dishe = (props) => {
  return (
    <div className="Dishe">
      <img src={props.image} alt={props.name} />
      <p>{props.name}</p>
      <p>{props.price} KGS</p>
      <NavLink to={"/edit/" + props.id} className="btn">
        Edit
      </NavLink>
      <button className="btn" onClick={props.remove}>
        Delete
      </button>
    </div>
  );
};

export default Dishe;
