import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Dishe from "../../Component/Dishe/Dishe";
import Spinner from "../../Component/UI/Spinner/Spinner";
import { deleteDishe, fetchDishes } from "../../store/actions/dishesAction";
import "./AdminPage.css";

const AdminPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchDishes());
  }, [dispatch]);

  const dishes = useSelector((state) => state.dishes.dishes);
  const loading = useSelector((state) => state.dishes.loading);

  const removeDishe = (id) => dispatch(deleteDishe(id));

  return (
    <div className="AdminPage">
      <NavLink to="/add" className="btn">
        Add new Dish
      </NavLink>
      {loading === true ? <Spinner /> : null}
      {Object.keys(dishes).map((dishe) => {
        return (
          <Dishe
            key={dishe}
            price={dishes[dishe].price}
            name={dishes[dishe].name}
            image={dishes[dishe].image}
            id={dishe}
            remove={() => removeDishe(dishe)}
          />
        );
      })}
    </div>
  );
};

export default AdminPage;
