import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Order from "../../Component/Order/Order";
import { fetchOrders } from "../../store/actions/ordersAction";

const Orders = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchOrders());
  }, [dispatch]);

  const dishes = useSelector((state) => state.dishes.dishes);
  const orders = useSelector((state) => state.orders.orders);

  const listOrders =
    orders &&
    dishes &&
    Object.keys(orders).map((id, index) => {
      let totalPrice = 150;

      const orderDetails = Object.keys(orders[id]).map((item) => {
        const name = dishes[item].name;
        const count = orders[id][item];
        const sum = dishes[item].price * orders[id][item];
        totalPrice += sum;

        return (
          <div key={id + index + name}>
            <p>
              {count} X {name}
              <b style={{ marginLeft: "40px " }}>{sum}KGS</b>
            </p>
          </div>
        );
      });

      return (
        <Order
          key={id + index}
          id={id}
          order={orderDetails}
          totalPrice={totalPrice}
        />
      );
    });

  return (
    <div className="Orders container">
      <h2>Orders</h2>
      {listOrders}
    </div>
  );
};

export default Orders;
