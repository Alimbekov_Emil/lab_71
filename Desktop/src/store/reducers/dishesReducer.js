import {
  DELETE_DISHE_FAILURE,
  DELETE_DISHE_REQUEST,
  DELETE_DISHE_SUCCESS,
  FETCH_DISHES_FAILURE,
  FETCH_DISHES_REQUEST,
  FETCH_DISHES_SUCCESS,
  FETCH_DISHE_FAILURE,
  FETCH_DISHE_REQUEST,
  FETCH_DISHE_SUCCESS,
  POST_DISHES_FAILURE,
  POST_DISHES_REQUEST,
  POST_DISHES_SUCCESS,
  PUT_DISHE_FAILURE,
  PUT_DISHE_REQUEST,
  PUT_DISHE_SUCCESS,
} from "../actions/dishesAction";

const initialState = {
  dishes: [],
  dishe: [],
  loading: false,
  erorr: false,
};

const dishesReduser = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DISHES_REQUEST:
      return { ...state, loading: true };
    case FETCH_DISHES_SUCCESS:
      return { ...state, loading: false, dishes: action.dishes };
    case FETCH_DISHES_FAILURE:
      return { ...state, loading: false, error: action.error };
    case POST_DISHES_REQUEST:
      return { ...state, loading: true };
    case POST_DISHES_SUCCESS:
      return { ...state, loading: false };
    case POST_DISHES_FAILURE:
      return { ...state, loading: false, error: action.error };
    case FETCH_DISHE_REQUEST:
      return { ...state, loading: true };
    case FETCH_DISHE_SUCCESS:
      return { ...state, loading: false, dishe: action.dishe };
    case FETCH_DISHE_FAILURE:
      return { ...state, loading: false, erorr: action.error };
    case DELETE_DISHE_REQUEST:
      return { ...state, loading: true };
    case DELETE_DISHE_SUCCESS:
      return { ...state, loading: false };
    case DELETE_DISHE_FAILURE:
      return { ...state, loading: false, error: action.error };
    case PUT_DISHE_REQUEST:
      return { ...state, loading: true };
    case PUT_DISHE_SUCCESS:
      return { ...state, loading: false };
    case PUT_DISHE_FAILURE:
      return { ...state, loading: false, error: action.error };
    default:
      return state;
  }
};

export default dishesReduser;
