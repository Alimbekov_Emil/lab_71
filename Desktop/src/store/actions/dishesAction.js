import axiosDishes from "../../axiosDishes";

export const FETCH_DISHES_REQUEST = "FETCH_DISHES_REQUEST";
export const FETCH_DISHES_SUCCESS = "FETCH_DISHES_SUCCESS";
export const FETCH_DISHES_FAILURE = "FETCH_DISHES_FAILURE";

export const POST_DISHES_REQUEST = "POST_DISHES_REQUEST";
export const POST_DISHES_SUCCESS = "POST_DISHES_SUCCESS";
export const POST_DISHES_FAILURE = "POST_DISHES_FAILURE";

export const FETCH_DISHE_REQUEST = "POST_DISHE_REQUEST";
export const FETCH_DISHE_SUCCESS = "POST_DISHE_SUCCESS";
export const FETCH_DISHE_FAILURE = "POST_DISHE_FAILURE";

export const PUT_DISHE_REQUEST = "PUT_DISHE_REQUEST";
export const PUT_DISHE_SUCCESS = "PUT_DISHE_SUCCESS";
export const PUT_DISHE_FAILURE = "PUT_DISHE_FAILURE";

export const DELETE_DISHE_REQUEST = "DELETE_DISHE_REQUEST";
export const DELETE_DISHE_SUCCESS = "DELETE_DISHE_SUCCESS";
export const DELETE_DISHE_FAILURE = "DELETE_DISHE_FAILURE";

export const fetchDishesRequest = () => ({ type: FETCH_DISHES_REQUEST });
export const fetchDishesSuccess = (dishes) => ({
  type: FETCH_DISHES_SUCCESS,
  dishes,
});
export const fetchDishesFailure = (error) => ({
  type: FETCH_DISHES_REQUEST,
  error,
});

export const postDishesRequest = () => ({ type: POST_DISHES_REQUEST });
export const postDishesSuccess = () => ({ type: POST_DISHES_SUCCESS });
export const postDishesFailure = (error) => ({
  type: POST_DISHES_FAILURE,
  error,
});

export const fetchDisheRequest = () => ({ type: FETCH_DISHE_REQUEST });
export const fetchDisheSuccess = (dishe) => ({
  type: FETCH_DISHE_SUCCESS,
  dishe,
});
export const fetchDisheFailure = (error) => ({
  type: FETCH_DISHE_REQUEST,
  error,
});

export const deleteDisheRequest = () => ({ type: DELETE_DISHE_REQUEST });
export const deleteDisheSuccess = () => ({ type: DELETE_DISHE_SUCCESS });
export const deleteDisheFailure = (error) => ({
  type: DELETE_DISHE_FAILURE,
  error,
});

export const putDisheRequest = () => ({ type: PUT_DISHE_REQUEST });
export const putDisheSuccess = () => ({ type: PUT_DISHE_SUCCESS });
export const putDisheFailure = (error) => ({ type: PUT_DISHE_FAILURE, error });

export const fetchDishes = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchDishesRequest());
      const response = await axiosDishes.get("dishes.json");
      dispatch(fetchDishesSuccess(response.data));
    } catch (e) {
      dispatch(fetchDishesFailure(e));
    }
  };
};

export const postDishes = (dishe) => {
  return async (dispatch) => {
    try {
      dispatch(postDishesRequest());
      await axiosDishes.post("dishes.json", dishe);
      dispatch(postDishesSuccess());
    } catch (e) {
      dispatch(postDishesFailure(e));
    }
  };
};

export const fetchDishe = (id) => {
  return async (dispatch) => {
    try {
      dispatch(fetchDisheRequest());
      const response = await axiosDishes.get("dishes/" + id + ".json");
      dispatch(fetchDisheSuccess(response.data));
    } catch (e) {
      dispatch(fetchDisheFailure(e));
    }
  };
};

export const deleteDishe = (id) => {
  return async (dispatch) => {
    try {
      dispatch(deleteDisheRequest());
      await axiosDishes.delete("dishes/" + id + ".json");
      dispatch(deleteDisheSuccess());
      dispatch(fetchDishes());
    } catch (e) {
      dispatch(deleteDisheFailure(e));
    }
  };
};

export const putDishe = (id, dishes) => {
  return async (dispatch) => {
    try {
      dispatch(putDisheRequest());
      await axiosDishes.put("dishes/" + id + ".json", dishes);
      dispatch(putDisheSuccess());
      dispatch(fetchDishes());
    } catch (e) {
      dispatch(putDisheFailure(e));
    }
  };
};
